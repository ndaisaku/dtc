var gulp = require('gulp');
var watch = require('gulp-watch');
var config = require('../config').watch;

gulp.task('watch', function () {
    


    watch(config.js,function(){
        gulp.start(['js']);
    });

    watch(config.plugin_js,function(){
        gulp.start(['plugin_js']);
    });

    watch(config.sass,function(){
    	gulp.start(['scss']);
    });
    
    watch(config.image,function(){
    	gulp.start(['image']);
    });
    watch(config.font,function(){
        gulp.start(['font']);
    });
    watch(config.www,function(){
    	gulp.start(['copy']);
    });
});