var gulp = require('gulp');
var config = require('../config').font;

gulp.task('font',function(){
	gulp.src(config.src)
		.pipe(gulp.dest(config.dest));
});