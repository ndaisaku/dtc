var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var config = require('../config').image;

gulp.task('image',function(){
	gulp.src(config.src)
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [
				{ removeViewBox: false },
				{ cleanupIDs: false }
			],
			use: [pngquant()]
		}))
		.pipe(gulp.dest(config.dest));
});