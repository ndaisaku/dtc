var gulp = require('gulp');
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var config = require('../config').plugin_js;

gulp.task('plugin_js',function(){
	gulp.src(config.src)
        .pipe(plumber())
        .pipe(concat(config.output))
		.pipe(gulp.dest(config.dest));
});