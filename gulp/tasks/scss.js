var gulp = require('gulp');
var sass = require('gulp-sass');
var gulpif = require('gulp-if');
var plumber = require('gulp-plumber');
var pleeease = require('gulp-pleeease');
var sourcemaps = require('gulp-sourcemaps');
 
var config = require('../config').scss;

gulp.task('scss', function () {
    gulp.src(config.src)
	    .pipe(sourcemaps.init())
		.pipe(plumber())
	    .pipe(sass())
	    .pipe(pleeease())
		.pipe(sourcemaps.write('.'))
    	.pipe(gulp.dest(config.dest)); 
});