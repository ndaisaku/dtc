var gulp = require('gulp');
var uglify = require('gulp-uglify');
var plumber = require('gulp-plumber');
var config = require('../config').js;


gulp.task('js',function(){
	gulp.src(config.src)
        .pipe(plumber())
        // .pipe(uglify())
		.pipe(gulp.dest(config.dest));
});