var path = require('path');
var dest = './build';
var src = './src';
var relativeSrcPath = path.relative('.',src);

module.exports = {

	dest: dest,

	//js
	js:{
		src: [src + '/js/normal/**'],
		dest: dest + '/assets/js/'
		// uglify: false
	},
	plugin_js:{
	    src: [
            src + '/js/plugin/gmaps.min.js',
            src + '/js/plugin/TweenMax.min.js',
            src + '/js/plugin/CustomEase.min.js',
            src + '/js/plugin/flickity.pkgd.min.js',
            src + '/js/plugin/flickity-imagesloaded.js',
            src + '/js/plugin/jquery.easing.1.3.js'
	    ],
	    dest: dest + '/assets/js',
	    output: 'plugin.js'
  	},
	// webpack: {
	// 	entry: [src + '/js/app.js'],
	// 	output:{
	// 		filename: 'function.js'
	// 	},
	// 	resolve: {
	// 		extensions: ['','.js']
	// 	}
	// },
	copy: {
		src:[
			src + '/www/**',
		],
		dest: dest
	},
	scss:{
		src: [
			src + '/scss/**/!(_)*'
		],
		dest: dest + '/assets/css/',
		autoprefixer:{
			browsers:['last 2 versions']
		},
		minify: false
	},
	image:{
		src: [
			src + '/images/**'
		],
		optimizationLevel:7,
		dest: dest + '/assets/images/'
	},
	font:{
		src: [
		  src + '/font/**'
		],
		dest: dest + '/assets/font/'
	},
	watch:{
		js: relativeSrcPath + '/js/**',
    	plugin_js: relativeSrcPath + '/js/plugin/**',
		sass: relativeSrcPath + '/scss/**',
		image: relativeSrcPath + '/images/',
		www: relativeSrcPath + '/www/**',
    	font: relativeSrcPath + '/font/**',
	}
};