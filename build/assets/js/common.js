$(function () {
    'use strict';

    //////////////////////////////////////////////////////////////////////////////////////
    //
    // V A R I A B L E S
    //
    //////////////////////////////////////////////////////////////////////////////////////

    var $fn = {},
        $el = {
            window       : $(window),
            html         : $('html'),
            body         : $('body'),
            document     : $('.l-document'),
            recruitSlide : ''
        },
        param = {
            winW        : window.innerWidth || $el.window.width(),
            winH        : window.innerHeight || $el.window.height(),
            docW        : $el.document.outerWidth(),
            docH        : $el.document.outerHeight(),
            switch_flg: false,
            flkty       : '',
            scrollpos   : 0,
            timer       : 0
        };

        var carousel;
        var $carousel_img;
        var carousel2;
        var $carousel_img2;

    $fn.mainImgSize = function () {
        if($('.main-carousel').length){
            var $target = $('.main-carousel');
            var $target2 = $('.main-carousel .flickity-viewport');

            var $target_parent = $carousel_img;
            var ratio = 0.71666666666;
            var mainImgW = $carousel_img.width();
            var mainImgH = mainImgW*ratio;
            $carousel_img.css('opacity',1);
            $target.css({
                height:mainImgH+'px'
            })

            $target2.css({
                height:mainImgH+'px'
            });
        }
    };

    $fn.mainImgSize2 = function () {
        if($('.main-carousel').length) {

            var $target = $('.menu-carousel');
            var $target2 = $('.menu-carousel .flickity-viewport');
            var $target_parent = $carousel_img;
            var ratio = 0.71666666666;
            var mainImgW = $carousel_img2.width();
            var mainImgH = mainImgW * ratio;
            $carousel_img2.css('opacity', 1);
            $target.css({
                height: mainImgH + 'px'
            })
            $target2.css({
                height: mainImgH + 'px'
            })
        }
    };

    $fn.indexSlider = function() {
        if ($('.main-carousel').length) {
            carousel = document.querySelector('.l-section-about .main-carousel');
            $carousel_img = $('.l-section-about .main-carousel .carousel-cell img');
            $fn.mainImgSize();
            param.flkty = new Flickity(carousel, {
                imagesLoaded: true,
                percentPosition: false,
                friction: .4,
                contain: false,
                prevNextButtons : false,
                pageDots: true,
                wrapAround: true
            });
        }
    };

    $fn.menuSlider = function() {
        if ($('.menu-carousel').length) {
            carousel2 = document.querySelector('.l-section-our_menu .menu-carousel');
            $carousel_img2 = $('.l-section-our_menu .menu-carousel .carousel-cell img');
            $fn.mainImgSize2();
            param.flkty = new Flickity(carousel2, {
                imagesLoaded: true,
                percentPosition: false,
                friction: .4,
                contain: false,
                prevNextButtons : false,
                pageDots: true,
                wrapAround: true
            });
        }
    };

    $fn.googlemaps = function () {
        if ($("#map").length) {
            var map_height = $("#map").width();
            $("#map").css('height',map_height+'px');
            var data_lat = 35.1605548;
            var data_lng = 136.9070817;
            var coordinates = {
                lat: data_lat,
                lng: data_lng
            };
            var zoom;

            if (param.winW <= 750) {
                zoom = 15;
            } else {
                zoom = 15;
            }

            var maps = new GMaps({
                div: '#map',
                zoom: zoom,
                lat: coordinates.lat,
                lng: coordinates.lng,
                scrollwheel: false,
                zoomControl: false,
                streetViewControl: false,
                mapTypeControl: false,
                overviewMapControl: false,
                resize: function () {
                    this.setCenter(coordinates);
                }

            });
            maps.addMarker({
                lat: coordinates.lat,
                lng: coordinates.lng,
//                        icon: "assets/images/map_icon.svg"

            });

            var styles = [
                {
                    "featureType": "all",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -120
                        },
                        {
                            "gamma": 1.0
                        }
                    ]
                }
            ];
            maps.addStyle({
                styledMapName: "Styled Map",
                styles: styles,
                mapTypeId: "map_style"
            });

            maps.setStyle("map_style");
        }

    };

    /* main Slide
   ============================================================== */
    // $fn.mainSlide = function () {
    //     var length = $('.l-section-main ul li').length;
    //     var count = length - 1;
    //     $('.l-section-main ul li').each(function (index) {
    //         $(this).css({
    //             'opacity':1
    //         });
    //
    //     });
    //     var object = $('.l-section-main ul li:eq('+ count +')');
    //     console.log(object);
    //     object.css({
    //         'opacity':1
    //     });
    //     var anim = TweenMax.to(object, 10, {ease:Power1.easeOut,scale:1.1});
    //     var anim2 = TweenMax.to(object, 3, {ease:Power1.easeOut,opacity:0,delay:3,
    //         onComplete  : function() {
    //             $fn.mainSlideRepaer(object , count);
    //         }
    //     });
    //
    // };
    // $fn.mainSlideRepaer = function (object , count) {
    //     count--;
    //     object = $('.l-section-main ul li:eq(' + count + ')');
    //     object.css({
    //         'zIndex':length,
    //         'opacity':1
    //     });
    //     var anim = TweenMax.to(object, 10, {ease:Power1.easeOut,scale:1.1});
    //     var anim2 = TweenMax.to(object, 3, {ease:Power1.easeOut,opacity:0,delay:3,
    //
    //         onComplete  : function() {
    //             $fn.mainSlideRepaer(object,count);
    //         }
    //     });
    //
    // };


    $('.l-header--inner-menu').on("click",function () {
        if( $('.nav').hasClass('open')){
            $(this).parent().parent().removeClass('open');

            var anim = TweenMax.staggerTo(".nav li .box", .8, {
                ease: Power3.easeOut, left: 0, delay: 0, onComplete: function () {
                    $('.nav').removeClass('open');
                    $('.nav .top-list li').css('display','none');
                }
            },0.1);


        }else{
            $('.nav').addClass('open');
            $('.nav').css({
                visibility:'visible'
            });
            $('.nav .top-list li').css('display','block');
            $(this).parent().parent().addClass('open');
            var anim = TweenMax.staggerTo(".nav li .box", .6, {ease:Power3.easeOut,left:"100%",delay:.3},0.1);
        }
    });

    $('.nav .l-content li').on("click",'a[href^=#]',function () {
        $('header').removeClass('open');

        var anim = TweenMax.staggerTo(".nav li .box", .8, {
            ease: Power3.easeOut, left: 0, delay: 0, onComplete: function () {
                $('.nav').removeClass('open');
                $('.nav .top-list li').css('display','none');
            }
        },0.1);
    });

    $fn.scrollNavi = function (scroll_amount) {
        var winHeight;
        if($('.l-section-main').length){
            winHeight = $('.l-section-main').height();
            if(param.scrollpos <= winHeight-100){
                $('.l-header--inner').addClass('nav--white');
                $('.l-header--inner').removeClass('nav--black');
            }else{
                $('.l-header--inner').removeClass('nav--white');
                $('.l-header--inner').addClass('nav--black');
            }
        }
        if($('.l-second-main').length){
            winHeight = $('.l-second-main').height();
            if(param.scrollpos <= winHeight){
                $('.l-header--inner').addClass('nav--white');
                $('.l-header--inner').removeClass('nav--black');
            }else{
                $('.l-header--inner').removeClass('nav--white');
                $('.l-header--inner').addClass('nav--black');
            }
        }



    };

    $fn.scrollNavi_load = function (scroll_amount) {
        var winHeight;
        if($('.l-section-main').length){
            winHeight = $('.l-section-main').height();
            if(scroll_amount <= winHeight){
                $('.l-header--inner').addClass('nav--white');
                $('.l-header--inner').removeClass('nav--black');
            }else{
                $('.l-header--inner').removeClass('nav--white');
                $('.l-header--inner').addClass('nav--black');
            }
        }
        if($('.l-second-main').length){
            winHeight = $('.l-second-main').height();
            if(scroll_amount <= winHeight){
                $('.l-header--inner').addClass('nav--white');
                $('.l-header--inner').removeClass('nav--black');
            }else{
                $('.l-header--inner').removeClass('nav--white');
                $('.l-header--inner').addClass('nav--black');
            }
        }


    };

    /* ************************************************
     * smoothScroll
     *
     * スムーススクロール
     *
    ************************************************* */
    $fn.smoothScroll = function () {
        $('body').on('click', 'a[href^=#]', function () {
            var href= $(this).attr("href");
            var target = $(href == "#" || href == "" ? 'html' : href);
            var position = target.offset().top;
            $('body,html').animate({scrollTop:position}, 1200, 'easeOutQuart');
            return false;
        });
    };

    /* scroll
    ============================================================== */
    $el.window.on('scroll touchmove', function () {
        // $fn.scrollparallax();
        var amount = $(this).scrollTop();
        $fn.scrollNavi(amount);
        clearTimeout( param.timer );
        param.timer = setTimeout(function() {
            param.scrollpos = amount;
        }, 10 );

    });



    // common
    $el.window.load(function () {
        $fn.smoothScroll();
        $fn.indexSlider();
        $fn.menuSlider();
        var amount = $el.window.scrollTop();
        $fn.scrollNavi_load(amount);
        // $fn.mainSlide();
        $fn.googlemaps();
    });
    $el.window.resize('load', function () {
        param = {
            winW        : window.innerWidth || $el.window.width(),
            winH        : window.innerHeight || $el.window.height(),
            docW        : $el.document.outerWidth(),
            docH        : $el.document.outerHeight(),
            switch_flg: false,
            flkty       : '',
            scrollpos   : 0,
            timer       : 0
        };

        $fn.mainImgSize();
        $fn.mainImgSize2();
    });
});

