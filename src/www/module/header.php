<header class="l-header">
    <div class="l-header--inner nav--white">
        <div class="l-header--inner-menu">
            <div class="border">
                <div class="top"></div>
                <div class="middle"></div>
                <div class="bottom"></div>
            </div>

            <div class="border-close">
                <div class="top"></div>
                <div class="middle"></div>
            </div>
        </div>
    </div>
</header>
<div class="nav">
    <section class="l-section l-content">
        <ul class="flex top-list">
            <li><a href="index.html#about" >ABOUT<span class="box"></span></a></li>
            <li><a href="index.html#menu">OUR MENU<span class="box"></span></a></li>
            <li><a href="index.html#shop">SHOP INFO<span class="box"></span></a></li>
            <li><a href="news.html">NEWS<span class="box"></span></a></li>
            <li><a href="contact.html">CONTACT<span class="box"></span></a></li>
        </ul>
    </section>
</div>